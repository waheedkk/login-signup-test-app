import { Router } from 'express';
import * as UserController from '../controllers/user.controller';
const router = new Router();

router.route('/signup').post(UserController.registerUser);

router.route('/login').post(UserController.loginUser);

router.route('/getuser/:userId').get(UserController.fetchUser);

export default router;
