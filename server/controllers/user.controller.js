import User from '../models/user';

/**
 * @export registerUser
 * @param {any} req
 * @param {any} res
 */
export function registerUser(req, res) {
  if (!req.body.user.userName || !req.body.user.email || !req.body.user.password) {
    res.status(500).json({err: "Some required field(s) are missing"});
    return;
  }

  User.findOne({userName: req.body.user.userName}).exec((err, user) => {
    if (err) {
      res.status(500).json({err: err});
      return;
    } else if (user) {
      res.status(400).json({err: "Username is already taken. Please Choose a different one."});
      return;
    } else {
      User.findOne({email: req.body.user.email}).exec((err, user) => {
        if (err) {
          res.status(500).json({err: err});
          return;
        } else if (user) {
          res.status(400).json({err: "Email is already in used by another account."});
          return;
        } else {
          const newUser = new User(req.body.user);
          newUser.save((err, saved) => {
            if (err) {
              res.status(500).json({err: err});
              return;
            }
            res.status(200).json({ 
              user: {
                _id: saved._id,
                userName: saved.userName,
                email: saved.email
              }
            });
            return;
          });
        }
      });
    }
  });
}

export function fetchUser(req, res) {
  if(req.params.userId) {
      User.findOne({ 
      $or: [ 
        { userName: req.params.userId }, 
        { email: req.params.userId }
      ]
    }).exec((err, user) => {
      if (err) {
        res.status(500).json({err: err});
        return;
      } else if(user) {
        res.status(200).json({ 
          user: {
            _id: user._id,
            userName: user.userName,
            email: user.email
          }
        });
        return;
      }
    });
  } 
  return false;
}

export function loginUser(req, res) {
  if (!req.body.user.userId || !req.body.user.password) {
    res.status(500).json({err: "Some required field(s) are missing"});
    return
  }

  User.findOne({ 
    $or: [ 
      { userName: req.body.user.userId }, 
      { email: req.body.user.userId }
    ]
  }).exec((err, user) => {
    if (err) {
      res.status(500).json({err: err});
      return;
    } else if (user) {
      user.comparePassword(req.body.user.password, (err, isMatched) => {
        if (err) {
          res.status(400).json({err: err});
          return;
        } else if (isMatched) {
          res.status(200).json({ 
            user: {
              _id: user._id,
              userName: user.userName,
              email: user.email
            }
          });
          return;
        } else {
          res.status(400).json({err: "Password is not Matched"});
          return;
        }
      });
    } else {
      res.status(404).json({err: "User is not found. Please Register Yourself"});
      return;
    }
  });
}
