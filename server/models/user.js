import mongoose from 'mongoose';
import bcrypt from 'bcrypt';
const salt = 8;
const Schema = mongoose.Schema;

const userSchema = new Schema({
  userName: { 
    type: 'String',
    required: true 
  },
  email: {
    type: 'String',
    required: true 
  },
  password: {
    type: 'String',
    required: true 
  },
  createdAt: {
    type: 'Date',
    default: Date.now
  }
});

// on every save, Hash the Password
userSchema.pre('save', function(next) {

  let user = this;

  // only hash the password if it has been modified (or is new)
  if (!user.isModified('password')) {
    return next();
  }

  // generate a salt
  bcrypt.genSalt(salt, function(err, salt) {
      if (err) {
        return next(err);
      }
      // hash the password using our new salt
      bcrypt.hash(user.password, salt, function(err, hash) {
          if (err) return next(err);

          // override the cleartext password with the hashed one
          user.password = hash;
          next();
      });
  });
});

userSchema.methods.comparePassword = function(clientClearTextPwd, callback) {
    bcrypt.compare(clientClearTextPwd, this.password, function(err, isMatched) {
        if (err) {
          callback(err);
        }
        callback(null, isMatched);
    });
};

export default mongoose.model('User', userSchema);
