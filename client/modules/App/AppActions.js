import callApi from '../../util/apiCaller';

// Export Constants
export const SET_USER = 'set_user';
export const SET_ERROR = 'set_error';

// Export Actions
export function setUser(user) {
  return {
    type: SET_USER,
    user
  };
}

// Export Actions
export function setError(err) {
  return {
    type: SET_ERROR,
    err
  };
}

export function registerUserRequest(user) {
  return (dispatch) => {
    return callApi('signup', 'post', user)
    .then((res) => {
        dispatch(setUser(res.user));
    }, (err) => {
        dispatch(setError(err))
    });
  };
}

export function loginRequest(loginCredentials) {
  return (dispatch) => {
    return callApi('login', 'post', loginCredentials)
    .then((res) => {
        dispatch(setUser(res.user));
    }, (err) => {
        dispatch(setError(err))
    });
  };
}

export function fetchUser(userId) {
  return (dispatch) => {
    return callApi('getuser/'+userId)
    .then((res) => {
        dispatch(setUser(res.user));
    }, (err) => {
        dispatch(setError(err))
    });
  };
}
