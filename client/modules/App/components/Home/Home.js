import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import { Grid, Row, Col } from 'react-bootstrap';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

// Import Style
import styles from './Home.css';


// Import Components
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import SignUp from '../SignUp/SignUp';
import LogIn from '../LogIn/LogIn';
import { isLoggedIn, getUser, getError } from '../../AppReducer';


class Home extends Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps && nextProps.user != {} && nextProps.isLoggedIn ) {
      window.location = "/" + nextProps.user.userName +"/dashboard";
    } else if (nextProps.err && nextProps.err.err){
      alert(nextProps.err.err);
    }
  }

  render() {
    return (
      <div>
        <div className={styles.container}>
          <Header />
            <div>
              <Grid>
                <Row className="show-grid">
                  <Col md={6}>
                      <MuiThemeProvider muiTheme={getMuiTheme()}>
                        <SignUp />
                      </MuiThemeProvider>
                  </Col>
                  <Col md={6}>
                      <MuiThemeProvider muiTheme={getMuiTheme()}>
                        <LogIn />
                      </MuiThemeProvider>
                  </Col>
                </Row>
              </Grid>
            </div>
          <Footer />
        </div>
      </div>
    );
  }
}

// Retrieve data from store as props
function mapStateToProps(state) {
  return {
    isLoggedIn: isLoggedIn(state),
    user: getUser(state),
    err: getError(state)
  };
}

export default connect(mapStateToProps)(Home);
