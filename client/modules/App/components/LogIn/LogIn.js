import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Panel } from 'react-bootstrap';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import callApi from '../../../../util/apiCaller';
import { loginRequest } from '../../AppActions';
import { isLoggedIn, getUser, getError } from '../../AppReducer';

class LogIn extends Component {

    constructor(props) {
      super(props);

      this.state = {
        userId: "",
        password: "",
        userIdErr: "",
        passwordErr: "",
      };
    }

    userIdChange(event) {
      this.setState({ userId: event.target.value });
    }

    passwordChange(event) {
      this.setState({ password: event.target.value });
    }

    onSubmit() {

      this.setState({
        userIdErr: "",
        passwordErr: ""
      });

      let userId = this.state.userId;
      let password = this.state.password;
      
      if( userId.trim() !== "" ) {
        if( password.trim() !== "" ) {
          let loginCredentials = {
            user: {
              userId: userId,
              password: password
            }
          };
          this.props.dispatch(loginRequest(loginCredentials));
        } else {
          this.setState({ passwordErr: "[Required]" });
        }
      } else {
        this.setState({ userIdErr: "[Required]" });
      }
    }

    componentDidMount(){
    }

    render(){
      return(
        <Panel>
          <div>
          <h3>LogIn to Test site</h3>
          <p><strong>Enter Username/Email and Password to log on:</strong></p>
        </div>
        <div>
          <TextField
            hintText="sam"
            floatingLabelText="Username/Email"
            value={this.state.userId}
            onChange={this.userIdChange.bind(this)}
            errorText={this.state.userIdErr}
          /><br />
          <TextField
            hintText="********"
            floatingLabelText="Password"
            type="password"
            value={this.state.password}
            onChange={this.passwordChange.bind(this)}
            errorText={this.state.passwordErr}
          /><br />
        </div>
        <br />
        <br />
        <RaisedButton label="LogIn" primary={true} fullWidth={true} onClick={this.onSubmit.bind(this)} />
      </Panel>
    );
  }
}

// Retrieve data from store as props
function mapStateToProps(state) {
  return {
    isLoggedIn: isLoggedIn(state),
    user: getUser(state),
    err: getError(state)
  };
}

LogIn.propTypes = {
  dispatch: PropTypes.func.isRequired
};

export default connect(mapStateToProps)(LogIn);