import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Panel, FormGroup } from 'react-bootstrap';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import { validateEmail } from '../../../../util/common';
import callApi from '../../../../util/apiCaller';
import { registerUserRequest } from '../../AppActions';
import { isLoggedIn, getUser, getError } from '../../AppReducer';

class SignUp extends Component {

    constructor(props) {
      super(props);
      this.state = {
        username: "",
        email: "",
        password: "",
        confirmPassword: "",
        usernameErr: "",
        emailErr: "",
        passwordErr: "",
        confirmPwdErr: ""
      };
    }

    usernameChange(event) {
      this.setState({ username: event.target.value });
    }

    emailChange(event) {
      this.setState({ email: event.target.value });
    }

    passwordChange(event) {
      this.setState({ password: event.target.value });
    }

    confirmPwdChange(event) {
      this.setState({ confirmPassword: event.target.value });
    }

    onSubmit() {

      this.setState({
        usernameErr: "",
        emailErr: "",
        passwordErr: "",
        confirmPwdErr: ""
      });

      let username = this.state.username;
      let email = this.state.email;
      let password = this.state.password;
      let confirmPwd = this.state.confirmPassword;
      
      if(username.trim() !== "") {
        if( validateEmail(email) ) {
          if(password.trim() !== "" && password.length >= 8) {
            if(confirmPwd === password) {
              let user = {
                user: {
                  userName: username,
                  email: email,
                  password: password
                }
              };
              this.props.dispatch(registerUserRequest(user));
            } else {
              this.setState({ confirmPwdErr: "Password is not Matched" });
            }
          } else {
            this.setState({ passwordErr: "Password must be 8 characters long" });
          }
        } else {
          this.setState({ emailErr: "Invalid Email" });
        }
      } else {
        this.setState({ usernameErr: "Username is Required" });
      }
    }

    componentDidMount() {
    }

    render(){
      return(
        <Panel>
          <div>
            <h3>Sign up now</h3>
            <p><strong>Fill in the form below to get instant access:</strong></p>
          </div>
          <div>
            <form>
              <FormGroup>
                <TextField
                  hintText="sam"
                  floatingLabelText="Username*"
                  value={this.state.username}
                  onChange={this.usernameChange.bind(this)}
                  errorText={this.state.usernameErr}
                /><br />
              </FormGroup>
              <FormGroup>
                <TextField
                  hintText="abc@any.xyz"
                  floatingLabelText="Email*"
                  value={this.state.email}
                  onChange={this.emailChange.bind(this)}
                  errorText={this.state.emailErr}
                /><br />
              </FormGroup>
              <FormGroup>
                <TextField 
                  hintText="********"
                  floatingLabelText="Password*"
                  type="password"
                  value={this.state.password}
                  onChange={this.passwordChange.bind(this)}
                  errorText={this.state.passwordErr}
                /><br />
              </FormGroup>
              <FormGroup>
                <TextField
                  hintText="********"
                  floatingLabelText="Confirm Password*"
                  type="password"
                  value={this.state.confirmPassword}
                  onChange={this.confirmPwdChange.bind(this)}
                  errorText={this.state.confirmPwdErr}
                /><br />
              </FormGroup>
            </form> 
          </div>
          <br />
          <br />
          <RaisedButton label="SignUp" primary={true} fullWidth={true} onClick={this.onSubmit.bind(this)}/>
        </Panel>
    );
  }
}

// Retrieve data from store as props
function mapStateToProps(state) {
  return {
    isLoggedIn: isLoggedIn(state),
    user: getUser(state),
    err: getError(state)
  };
}

SignUp.propTypes = {
  dispatch: PropTypes.func.isRequired
};

export default connect(mapStateToProps)(SignUp);