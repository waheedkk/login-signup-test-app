import React from 'react';

// Import Style
import styles from './Header.css';

export function Header(props, context) {
  // const languageNodes = props.intl.enabledLanguages.map(
  //   lang => <li key={lang} onClick={() => props.switchLanguage(lang)} className={lang === props.intl.locale ? styles.selected : ''}>{lang}</li>
  // );

  return (
    <div>
      <div className={styles.content}>
        <h1 className={styles['site-title']}>
          Node.js + React.js SignUp/LogIn Test App 
        </h1>
      </div>
    </div>
  );
}

// Header.contextTypes = {
//   router: React.PropTypes.object,
// };

// Header.propTypes = {
//   toggleAddPost: PropTypes.func.isRequired,
//   switchLanguage: PropTypes.func.isRequired,
//   intl: PropTypes.object.isRequired,
// };

export default Header;
