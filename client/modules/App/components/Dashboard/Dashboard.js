import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { AppBar, ListItem, Avatar } from 'material-ui';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import { isLoggedIn, getUser, getError } from '../../AppReducer';
import { fetchUser } from '../../AppActions';

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {}
    }
  }
  
  componentDidMount() {
  }

  render() {
    return (
      <div>
        <MuiThemeProvider muiTheme={getMuiTheme()}>
          <AppBar 
            title="Test App"
            iconElementRight={
              <ListItem
                disabled={true}
                leftAvatar={
                  <Avatar src="" />
                }
              >
                {this.props.user.userName ? this.props.user.userName : "Avatar"}
              </ListItem>}
          />
        </MuiThemeProvider>
      </div>
    );
  }
}

// Actions required to provide data for this component to render in server side.
Dashboard.need = [params => {
  return fetchUser(params.userId);
}];

// Retrieve data from store as props
function mapStateToProps(state) {
  return {
    isLoggedIn: isLoggedIn(state),
    user: getUser(state),
    err: getError(state)
  };
}

export default connect(mapStateToProps)(Dashboard);
