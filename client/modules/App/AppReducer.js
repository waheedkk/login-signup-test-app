// Import Actions
import { SET_USER, SET_ERROR } from './AppActions';

// Initial State
const initialState = {
  isLoggedIn: false,
  user: {},
  err: {}
};

const AppReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER:
      return {
        isLoggedIn: true,
        user: action.user,
        err: {}
      };

    case SET_ERROR:
      return {
        isLoggedIn: false,
        user: {},
        err: action.err
      };

    default:
      return state;
  }

};

export const isLoggedIn = state => state.app.isLoggedIn;

export const getUser = state => state.app.user;

export const getError = state => state.app.err;
// Export Reducer
export default AppReducer;
